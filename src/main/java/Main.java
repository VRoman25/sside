import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.*;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Semaphore;

public class Main {
    public static String authToken;
    public static String appId = "appID";

    public static void main(String[] args) {
        final Semaphore sem = new Semaphore(0);
        FirebaseOptions options = null;
        try {
            options = new FirebaseOptions.Builder()
                    .setServiceAccount(new FileInputStream(
                            "вставить сюда имя service account json например project-ccc21123fd12f14f.json"))
                    .setDatabaseUrl(
                            "Вставить сюда ссылку на firebase project например https://project-14673453453342342.firebaseio.com/")
                    .build();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FirebaseApp.initializeApp(options);
        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                DatabaseReference ref = database.getReference("delivery");
                ref.addValueEventListener(new ValueEventListener() {
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            Iterable<DataSnapshot> childs = dataSnapshot.getChildren();
                            for (DataSnapshot child : childs) {
                                String str = child.getValue().toString();
                                String[] strarr = str.split(";");
                                try {
                                    httpDelivery(strarr[0], strarr[1], strarr[2]);
                                    System.out.println("delivery: " + strarr[0] + ";" + strarr[1] + ";" + strarr[2]);
                                    child.getRef().removeValue();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    public void onCancelled(DatabaseError databaseError) {}
                });
            }
        };

        Runnable runnableRegisterDevice = new Runnable() {
            @Override
            public void run() {
                DatabaseReference ref = database.getReference("registration");
                ref.addValueEventListener(new ValueEventListener() {
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            Iterable<DataSnapshot> childs = dataSnapshot.getChildren();
                            for (DataSnapshot child : childs) {
                                String[] strLine = child.getValue().toString().split(";");
                                String op = strLine[0];
                                String fcmToken = strLine[1];
                                String phone = strLine[2];
                                System.out.println(op + ";" + phone + ";" + fcmToken);
                                try {
                                    switch (op) {
                                        case "reg":
                                            httpRegDevice(fcmToken, phone);
                                            break;
                                        case "update":
                                            httpUpdDevice(fcmToken, phone);
                                            break;
                                        case "unreg":
                                            httpUnRegDevice(phone);
                                            break;
                                        case "rereg":
                                            httpUnRegDevice(phone);
                                            httpRegDevice(fcmToken, phone);
                                            break;
                                    }
                                    child.getRef().removeValue();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    public void onCancelled(DatabaseError databaseError) {}
                });
            }
        };

        DatabaseReference ref = database.getReference("settings/authToken");
        ref.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    authToken = dataSnapshot.getValue().toString();
                    new Thread(run).start();
                    new Thread(runnableRegisterDevice).start();
                }
            }
            public void onCancelled(DatabaseError databaseError) {}
        });

        System.out.println("start");
        try {
            sem.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String httpDelivery(String sdUuid, String sdLogin, String status) throws Exception {
        URL url = new URL("http://sds2.intervale.ru/pushes/api/push/delivered");
        HttpURLConnection conn =
                (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setAllowUserInteraction(false);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Bearer " + authToken);

        OutputStream out = conn.getOutputStream();
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.write("{\"sdUuid\" : \""
                + sdUuid + "\", \"sdLogin\" : \""
                + sdLogin + "\", \"status\" : "
                + status + "}");
        writer.close();
        out.close();

        if (conn.getResponseCode() != 200) {
            throw new IOException(conn.getResponseMessage());
        }

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();
        conn.disconnect();
        System.out.println(sb.toString());
        return sb.toString();
    }

    public static String httpRegDevice(String fcmToken, String phone) throws Exception {
        URL url = new URL("https://sds2.intervale.ru/pushes/api/app/token");
        HttpURLConnection conn =
                (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setAllowUserInteraction(false);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Bearer " + authToken);

        OutputStream out = conn.getOutputStream();
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.write("{\"token\":\""+ fcmToken +"\"," +
                "\"appId\":\"" + appId + "\"," +
                "\"auth\":[{\"id\":\"" + phone + "\"}]," +
                "\"checkSum\":\"" + phone + "111\"}");

        writer.close();
        out.close();

        if (conn.getResponseCode() != 200) {
            if (conn.getResponseMessage().equals("Unauthorized")) {
                if (httpRegServ()) httpRegDevice(fcmToken, phone);
            }
            return "";
        }
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();
        conn.disconnect();
        System.out.println(sb.toString());
        return sb.toString();

    }

    public static String httpUpdDevice(String fcmToken, String phone) throws Exception {
        URL url = new URL("https://sds2.intervale.ru/pushes/api/app/token/update");
        HttpURLConnection conn =
                (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setAllowUserInteraction(false);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Bearer " + authToken);

        OutputStream out = conn.getOutputStream();
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.write("{\"appId\":\"" + appId + "\"," +
                "\"token\":\""+ fcmToken +"\"," +
                "\"msisdn\":\"" + phone + "\"}");

        writer.close();
        out.close();

        if (conn.getResponseCode() != 200) {
            if (conn.getResponseMessage().equals("Unauthorized")) {
                if (httpRegServ()) httpUpdDevice(fcmToken, phone);
            }
            return "";
        }
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();
        conn.disconnect();
        System.out.println(sb.toString());
        return sb.toString();
    }

    public static String httpUnRegDevice(String phone) throws Exception {
        URL url = new URL("http://sds2.intervale.ru/pushes/api/app/token/delete");
        HttpURLConnection conn =
                (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setAllowUserInteraction(false);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Bearer " + authToken);

        OutputStream out = conn.getOutputStream();
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.write("{\"msisdn\":\""+ phone +"\", \"appId\":\"" + appId + "\"}");
        writer.close();
        out.close();

        if (conn.getResponseCode() != 200) {
            throw new IOException(conn.getResponseMessage());
        }

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();
        conn.disconnect();
        System.out.println(sb.toString());
        return sb.toString();
    }

    public static boolean httpRegServ() throws Exception {
        URL url = new URL("https://sds2.intervale.ru/pushes/api/login");
        HttpURLConnection conn =
                (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setAllowUserInteraction(false);
        conn.setRequestProperty("Content-Type", "application/json");

        String login = "login";
        String pass = "pass";

        OutputStream out = conn.getOutputStream();
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.write("{\"login\":\""+ login +"\", \"password\":\"" + pass + "\"}");
        writer.close();
        out.close();

        if (conn.getResponseCode() != 200) {
            return false;
        }

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();
        conn.disconnect();
        System.out.println("regisred in the service" + sb.toString());
        authToken = new JSONObject(sb.toString()).getString("token");
        DatabaseReference res = FirebaseDatabase.getInstance().getReference("settings/authToken");
        res.setValue(authToken);
        return true;
    }
}